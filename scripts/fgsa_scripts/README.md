# FGSA_scripts

R scripts to perform Functional Gene-Set Analyses after a RNA-Seq or microarray differential expression study. Three methods are currently available:

 - ORA (over-representation analysis) using a Fisher exact test,
 - CAMERA from the limma R package,
 - the famous GSEA using the fgsea R package.

Several packages are required to execute the main `run_FGSA()` function. They can be installed with:

```
if (!requireNamespace("BiocManager", quietly = TRUE)) install.packages("BiocManager")
# packages used by all the methods
install.packages("ggplot2")
install.packages("dplyr")
install.packages("tidyr")
install.packages("readr")
install.packages("tibble")
install.packages("purrr")
install.packages("stringr")
# to use the CAMERA method
BiocManager::install("limma")
# to use the fgsea method
BiocManager::install("fgsea")
install.packages("pheatmap")
install.packages("RColorBrewer")
# only to compare FGSA methods:
install.packages("GGally")
install.packages("UpSetR")
install.packages("VennDiagram")
```

The `toy_example.R` file shows (i) how to prepare the data (gene-sets, gene IDs, results of the differential analysis...) and (ii) how to use the functions to perform a gene-set level analysis. It requires the `toy_example.RData` file that stores the results of a RNA-Seq differential analysis.
