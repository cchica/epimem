# Bioinformatic and biostatistic analysis for EpiMem 

This repository contains the data, scripts and result files used/obtained in the bioinformatic and biostatistic downstream analysis, after raw data analysis and quantification, for the following publication:

Epithelial cells maintain memory of prior infection with Streptococcus pneumoniae through di-methylation of histone H3.

Christine Chevalier (1), Claudia Chica (2), Justine Matheau (1,3), Adrien Pain (2), Michael G. Connor (1), Melanie A. Hamon (1*).

Affiliations
1 Chromatin and Infection Laboratory, Institut Pasteur, Paris, France.
2 Institut Pasteur, Université de Paris Cité, Bioinformatics and Biostatistics Hub, F-75015 Paris, France.
3 Université Paris Cité, 562 Bio Sorbonne Paris Cité, Paris, France.

*Correspondence:
Melanie A. Hamon (melanie.hamon@pasteur.fr)

## Abstract
Epithelial cells are the first point of contact for bacteria entering the respiratory tract.  Streptococcus pneumoniae is an obligate human pathobiont of the nasal mucosa, carried asymptomatically but also the cause of severe pneumoniae. The role of the epithelium in maintaining homeostatic interactions or mounting an inflammatory response to invasive S. pneumoniae is currently poorly understood. However, studies have shown that chromatin modifications, at the histone level, induced by bacterial pathogens interfere with the host transcriptional program and promote infection. In this study, we uncover a histone modification induced by  S. pneumoniae infection that is maintained  for at least 9 days upon clearance of bacteria with antibiotics. Di-methylation of histone H3 on lysine 4 (H3K4me2) is induced in an active manner by bacterial attachment to host cells. We show that infection establishes a unique epigenetic program affecting the transcriptional response of epithelial cells, rendering them more permissive upon secondary infection. Our results establish H3K4me2 as a unique modification induced by infection, distinct from H3K4me3 or me1, which localizes to enhancer regions genome-wide. Therefore, this study reveals evidence that bacterial infection leaves a memory in epithelial cells after bacterial clearance, in an epigenomic mark, thereby altering cellular responses to subsequent infections. 

## Keywords
Epigenetics; H3K4me2; Histone modification; Streptococcus pneumoniae; bacterial infection; Epigenome; Transcriptome; Multiple Factor Analysis.
 


## Authors and acknowledgment
Analysis where performed by Adrien Pain (apain@pasteur.fr) and Claudia Chica (cchica@pasteur.fr), in close collaboration with Christine Chevalier and Mélanie Hamon.
Claudia and Adrien would like to thank the members of the Genome Organization Regulation and Expression expertise group, of the Bioinformatics and Biostatistics Hub of the Institut Pasteur, for useful discussions and their feedback. 
